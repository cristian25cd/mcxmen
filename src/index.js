import express from 'express'
import { getDnaStats, isMutant } from './logic/mutant.js'
import bodyParser from 'body-parser'

// servidor express configurado
const app = express()
const port = process.env.PORT || 3000
app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.post('/mutant', async(req, res) => {
    let is = await isMutant(req.body.dna)
    if (is) {
        res.status(200).send()
    } else {
        res.status(403).send()
    }
})

app.get('/stats', async(req, res) => {
    let stats = await getDnaStats()
    res.send(stats[0])
})

app.listen(port, () => {
    console.log(`MCXMEN listening on port ${port}`)
})

export default app