import { Dna } from '../persistence/classes/dna';
/**
 * Funcion que determina si el adn ingresado es el de un mutante
 * @param {String[]} dna arreglo con las bases nitrogenadas
 * @pre dna es un arreglo de strings formado unicamente con las letras 'A', 'C', 'G', 'T'
 * @returns true si el arreglo representa un mutante o false de lo contrario
 */
export async function isMutant(dna) {
    let result = false;

    //Convertir arreglos a matriz
    let matrix = dna.map((a) => {
        return a.split('');
    });

    //Iterar parcialmente la matriz hasta encontrar una secuencia en la que sea mutante
    for (let i = 0; i < matrix.length && !result; i++) {
        for (let j = 0; j < matrix[i].length && !result; j++) {
            let bn = matrix[i][j];
            let k = 1;
            let search = true;

            //Revisar en dirección de la columna si se repite la base nitrogenada
            while (search && i + k < matrix.length && k < 4) {
                if (bn !== matrix[i + k][j]) {
                    search = false;
                } else {
                    k++;
                }
            }
            if (k === 4) {
                result = true;
            } else {
                k = 1;
            }

            //Revisar en dirección de la fila si se repite la base nitrogenada
            while (search && j + k < matrix[i].length && k < 4) {
                if (bn !== matrix[i][j + k]) {
                    search = false;
                } else {
                    k++;
                }
            }
            if (k === 4) {
                result = true;
            } else {
                k = 1;
            }

            //Revisar en dirección de la diagonal si se repite la base nitrogenada
            while (
                search &&
                i + k < matrix.length &&
                j + k < matrix[i].length &&
                k < 4
            ) {
                if (bn !== matrix[i + k][j + k]) {
                    search = false;
                } else {
                    k++;
                }
            }
            if (k === 4) {
                result = true;
            } else {
                k = 1;
            }
        }
    }

    //Persist dna and result
    await Dna.create({
        dna: dna,
        isMutant: result,
    }).catch((error) => {
        console.log(error);
    });

    return result;
}

export async function getDnaStats() {
    return await Dna.aggregate([{
                $facet: {
                    count_mutant_dna: [{ $match: { isMutant: true } }],
                    count_human_dna: [{ $match: { isMutant: false } }],
                },
            },
            {
                $project: {
                    count_mutant_dna: {
                        $size: '$count_mutant_dna',
                    },
                    count_human_dna: {
                        $size: '$count_human_dna',
                    },
                    ratio: {
                        $cond: [{
                            $eq: [{
                                $sum: [
                                    { $size: '$count_mutant_dna' },
                                    { $size: '$count_human_dna' }
                                ]
                            }, 0]
                        }, 0, {
                            $divide: [{ $size: '$count_mutant_dna' }, { $sum: [{ $size: '$count_mutant_dna' }, { $size: '$count_human_dna' }] }]
                        }]
                    }
                },
            },
        ])
        .exec()
}