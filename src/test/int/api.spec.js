import supertest from 'supertest';
import app from '../..';

let mdna = ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG']; //true
let dna = ['AAATTT', 'GGGCCC', 'GGGAAA', 'TTTAAA', 'AAACCC', 'GGGTTT']; //false

let api = supertest(app);

describe('Api test suite', () => {

    //Prueba que el api esté funcionando
    it('Test api up', async() => {
        console.log('Test init')
        let response = await api.get('/');
        expect(response.text).toEqual('Hello World!');
        expect(response.statusCode).toBe(200);
        console.log('finished 1', response.statusCode)
    });

    //Prueba que la base de datos esté vacia y el servicio de stats funcione
    it('Test stats endpoint', async() => {
        let response = await api.get('/stats');
        expect(response.statusCode).toBe(200);
        expect(response.body.count_mutant_dna).toBe(0);
        expect(response.body.count_human_dna).toBe(0);
        expect(response.body.ratio).toBe(0);
        console.log('finished 2', response.statusCode)
    });

    //Prueba el endpoint de /mutant y la persistencia en base de datos correctamente
    it('Test Mutant endpoint', async() => {
        let response = await api.post('/mutant').send({ dna: mdna });
        expect(response.statusCode).toBe(200);
        console.log('finished 3', response.statusCode)
        response = await api.get('/stats');
        expect(response.body.count_mutant_dna).toBe(1);
        expect(response.body.count_human_dna).toBe(0);
        expect(response.body.ratio).toBe(1);
        console.log('finished 4', response.statusCode)
    });

    //Prueba que no se ingresen duplicados en la base de datos
    it('Test solo un registro', async() => {
        let response = await api.post('/mutant').send({ dna: mdna });
        expect(response.statusCode).toBe(200);
        console.log('finished 5', response.statusCode)
        response = await api.get('/stats');
        expect(response.statusCode).toBe(200);
        expect(response.body.count_mutant_dna).toBe(1);
        expect(response.body.count_human_dna).toBe(0);
        expect(response.body.ratio).toBe(1);
        console.log('finished 6', response.statusCode)
    });

    //Prueba el endpoint de /stats
    it('Test stats', async() => {
        let response = await api.post('/mutant').send({ dna: dna }); //Tests is human
        expect(response.statusCode).toBe(403);
        console.log('finished 7', response.statusCode)

        response = await api.get('/stats');
        expect(response.statusCode).toBe(200);
        expect(response.body.count_mutant_dna).toBe(1);
        expect(response.body.count_human_dna).toBe(1);
        expect(response.body.ratio).toBe(0.5);
        console.log('finished 8', response.statusCode)
    });

});