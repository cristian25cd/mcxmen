import mongoose from "mongoose"
import db from "../db";

//Schema
const dnaSchema = new mongoose.Schema({
    dna: [String],
    createdAt: {
        type: Date,
        default: Date.now
    },
    isMutant: Boolean,
})

//Hooks
dnaSchema.pre('save', function(next) {
    Dna.findOne({ dna: this.dna }).select('_id').exec((err, doc) => {
        if (err) {
            console.log(err)
        } else if (doc) {
            next('Already exists a dna')
        }
        next();
    })
});

//Se puede usar para extender el modelo de dna
// class DnaClass {

//     getDnaStats() {

//     }
// }
// dnaSchema.loadClass(DnaClass)

//Modelo para ADN
export const Dna = db.model('dna', dnaSchema);