import mongoose from 'mongoose'

const user = process.env.MONGO_USER
const password = process.env.MONGO_PW
const server = process.env.MONGO_URL || 'mongo:27017'
const name = process.env.MONGO_DB

//Conexion a DB
console.log('Mongo db' + server + ' ' + name)
const db = await mongoose.connect(`mongodb${server.includes(':')?'':'+srv'}://${user}:${password}@${server}/${name}`);

if (name == 'test') {
    db.connection.dropCollection('dnas')
}

export default db