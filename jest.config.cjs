module.exports = {
    moduleFileExtensions: [
        "jsx",
        "js",
        "json"
    ],
    testEnvironment: "jest-environment-node",
    transform: {},
    testPathIgnorePatterns: [
        "node_modules/?!(react-native|native-base)"
    ],
    testTimeout: 70000

}