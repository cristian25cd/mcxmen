# MCXmen

Esta app puede ser probada usando [este link](https://mcxmen.herokuapp.com).

- [POST] **/mutant** detecta si un humano es mutante.
- [GET] **/stats** devuelve un Json con las estadísticas de las verificaciones de ADN.

## Pre requisitos
- Node.js
- Docker Desktop (opcional)

## Correr localmente

El app puede ser ejecutada localmente con los siguientes pasos:
### Clonar el proyecto
```
git clone https://gitlab.com/cristian25cd/mcxmen.git
```
### Instalar dependencias
```
npm install
```
### Iniciar aplicación localmente 
```
npm run app
```
El servidor se levantará en **localhost:3000** y utilizará la base de datos configurada en el archivo **.env**.

## Correr localmente con docker
El servidor se ha configurado para ser ejecutado en docker junto con una instancia de mongo.
```
docker-compose up --build
```
## Correr los test
```
npm run test
```